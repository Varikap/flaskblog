# ovo ce importovati iz __init__ iz flaskblog paket
from flaskblog import create_app

"""
    Mozes ovako da run app
        export FLASK_APP=/route/to/.pyfile
        export FLASK_DEBUG=1
    i onda pokreces server sa komandom
        flask run
"""

app = create_app()

if __name__ == "__main__":
    """
    A mozes da odradis i ovo
    i onda pokreces sa python flaskblog.py
    """
    app.run(debug=True)

