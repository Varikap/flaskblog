import os
import secrets
from PIL import Image
from flask import url_for, current_app
from flask_mail import Message
from flaskblog import mail


def save_picture(form_picture):
    # 8 bytes
    random_hex = secrets.token_hex(8)
    f_name, f_ext = os.path.splitext(form_picture.filename)
    # posle pokupljene extenzije uzimam random + extenzija da ne bi bilo slika istog imena
    picture_file_name = random_hex + f_ext
    picture_path = os.path.join(current_app.root_path, 'static/profile_pics', picture_file_name)

    output_size = (169,169)
    # kreira sliku iz form_picture
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    # cuva sliku na taj path
    i.save(picture_path)
    return picture_file_name


def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Password reset request',
                  sender='noreply@demo.com', recipients=[user.email])
    # external true mora biti ovde da bi ga odveo na pravi link, bez toga su samo relativni linkovi
    msg.body = f''' To reset your password, visit the following link: 
{ url_for('users.reset_token', token=token, _external=True) }

If you did not make this request then simply ignore this email and no changes will be made.
    '''
    # mail importovan iz flaskblog
    mail.send(msg)