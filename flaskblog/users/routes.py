from flask import render_template, url_for, flash, redirect, request, Blueprint
from flask_login import login_user, current_user, logout_user, login_required
from flaskblog import db, bcrypt
from flaskblog.models import User, Post
from flaskblog.users.forms import (RegistrationForm, LoginForm, UpdateAccountForm,
                                   RequestResetForm, ResetPasswordForm)
from flaskblog.users.utils import save_picture, send_reset_email


users = Blueprint('users', __name__)


@users.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            # importovan od flask_login
            login_user(user, remember=form.remember.data)
            # ako next parametar postoji prosledice sa logina na taj next parametar
            # tipa neulogovan pristupi /account za koji ti treba login, odvede ga na /login
            # i kad se uloguje vrati ga na /account
            next_page = request.args.get('next')
            # redirect next_page if next_page exist or its not false else home
            return redirect(next_page) if next_page else redirect(url_for('main.home'))
        #ovo ce zviznuti na login.html importovani deo od layout.html
        #coolstuff
        flash('Login unsuccessful', 'danger')
    return render_template('login.html', form=form, title='Login')


# govori koje metode prihvata metoda
@users.route("/register", methods=['GET','POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    #kreiranje naseg form i saljemo ga blocku na register.html
    form = RegistrationForm()
    if form.validate_on_submit():
        #generise hash iz password submit polja decode utf mora da ne vrati bytes
        hashed_pass = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_pass)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in!', 'success')
        return redirect(url_for('users.login'))
    return render_template('register.html', form=form, title='Register')


@users.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('main.home'))


@users.route("/account", methods=['GET','POST'])
@login_required
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        # posto slika nema DataRequired moras proveriti da li postoji ovde
        if form.picture.data:
            # uzmes path slike i sacuvas je na tog usera onda
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash('Your account has been updated', 'success')
        return redirect(url_for('users.account'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    image_file = url_for('static', filename='profile_pics/' + current_user.image_file)
    return render_template('account.html', title='Account', image_file=image_file, form=form)


@users.route("/user/<string:username>")
def user_posts(username):
    page = request.args.get('page',1,type=int)
    per_page = request.args.get('per-page',5,type=int)
    # ako ne nadje usera vratice 404 :D
    user = User.query.filter_by(username=username).first_or_404()
    # filteruje za tog usera
    posts = Post.query.filter_by(author=user)\
                    .order_by(Post.date_posted.desc())\
                    .paginate(per_page=per_page, page=page)
    return render_template('user_posts.html', posts=posts, user=user)


@users.route("/reset_password", methods=['GET','POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        flash('Email has been sent with instructions to reset your password', 'info')
        return redirect(url_for('users.login'))
    return render_template('reset_request.html', title='Reset Password', form=form)


@users.route("/reset_password/<token>", methods=['GET','POST'])
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    user = User.verify_reset_token(token)
    if user is None:
        flash('That is an invalid or expired token', 'warning')
        return redirect(url_for('reset_request'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        # nema potrebe za add part jer menajmo password samo commit onda
        db.session.commit()
        flash('Your password has been updated! You are now able to log in!', 'success')
        return redirect(url_for('users.login'))
    return render_template('reset_token.html', title='Reset Password', form=form)

'''
    flash mesagges:
    one se uglavljuju u coockie i kad saljemo sledeci request,
    sa  {% with messages = get_flashed_messages(with_categories=true) %}
    kupimo poruku i jos jedan prosledjen string kao kategoriju poruke,
    Moze i bez kategorije, onda se ovde gore ne prosledjuje parametar with_categories
'''