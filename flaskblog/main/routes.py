from flask import render_template, request, Blueprint
from flaskblog.models import Post

main = Blueprint('main', __name__)


@main.route("/")
@main.route("/home") #obe putanje daju isti metod
def home():
    # ako u req postoji atribut page ili per-page pokupice samo ako je int
    # u suprotnom izbacuje error, ako ga nema stavice default od 1 i 5, redom
    page = request.args.get('page',1,type=int)
    per_page = request.args.get('per-page',5,type=int)
    # date desc() baby! :D
    posts = Post.query.order_by(Post.date_posted.desc()).paginate(per_page=per_page, page=page)
    return render_template('home.html', posts=posts)


@main.route("/about")
def about():
    return render_template('about.html', title='About')


@main.route("/proba")
def proba():
    return render_template('proba.html')

