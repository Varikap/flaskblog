from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_mail import Mail
from flaskblog.config import Config


db = SQLAlchemy()
bcrypt = Bcrypt()
# sam ce da przi sesije
login_manager = LoginManager()
# ako neko pokusa da pristupi funkciji koja ima @login_required dekorator, odvesce ga na login
# i poslace flash message zato ovdee treba ime funkcije koja ce da obradi login
login_manager.login_view = 'users.login'
# ova sprava sama moze da krkne flash message, ovde samo dajemo kategoriju info kao bootstrap klasu
login_manager.login_message_category = 'info'

mail = Mail()




def create_app(config_class=Config):
    app = Flask(__name__)
    # config uzme iz config.py
    app.config.from_object(Config)

    db.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)

    # moras importovati rute ovde da bi app znala da ih pokrece, ali tek dole import ide
    # da ne bi usli u recursive import
    # importujemo instancu! Blueprinta i onda ih registrujemo da app zna da su rute
    from flaskblog.users.routes import users
    from flaskblog.posts.routes import posts
    from flaskblog.main.routes import main
    from flaskblog.errors.handlers import errors
    app.register_blueprint(users)
    app.register_blueprint(posts)
    app.register_blueprint(main)
    app.register_blueprint(errors)


    return app