from flask import Blueprint, render_template

errors = Blueprint('erros', __name__)

# na prostoru app svaki 404 ce da obradi ova funkija, mora da prima error
@errors.app_errorhandler(404)
def error_404(error):
    # ovde mora da se specifira 404 jer po defaultu vraca 200
    return render_template('errors/404.html'), 404

@errors.app_errorhandler(403)
def error_403(error):
    return render_template('errors/403.html'), 403


@errors.app_errorhandler(500)
def error_500(error):
    return render_template('errors/500.html'), 500


"""
    Razlika izmedju app_errorhandler i errorhandler je
    sto 2. vazi samo za taj blueprint ovaj vazi za sve
"""