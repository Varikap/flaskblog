import os


class Config():
    # random generisan broj za neki kuki thsi
    SECRET_KEY = '258abdb0c2c5a81b4b5e2edd52b81b16'
    # /// znaci trenutna lokacija ovog fajla
    SQLALCHEMY_DATABASE_URI = 'sqlite:///site.db'
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = os.environ.get('EMAIL_USER')
    MAIL_PASSWORD = os.environ.get('EMAIL_PASS')